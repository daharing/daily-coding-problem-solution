/*
 * Author:  Dustin Haring
 * Date:    March 31, 2020
 * Comment: This file is part of the Daily Coding Problem C solutions
 * Resource: 
 *          https://www.geeksforgeeks.org/serialize-deserialize-binary-tree/
 *          https://www.geeksforgeeks.org/tree-traversals-inorder-preorder-and-postorder/?ref=rp
 */


#include "Problem_003.h" //This include is for testing purposes only and is not part of the solution. It contains the Node struct definition
/**
 * Definition for a binary tree node (also defined in Problem_003.h)
 * struct Node {
 *     int val;
 *     struct Node *left;
 *     struct Node *right;
 * };
 */






/*-------------------------------------
|         PUBLIC DECLARATIONS          |
--------------------------------------*/

#include <stdlib.h>  //For NULL and malloc
#include <stdbool.h> //For bools
#include <limits.h>  //For INT_MIN
#include <assert.h>

#define NULL_MARKER (INT_MIN) // Because it is unlikely that INT_MIN will ever be used, I chose it to be my NULL node indicator

typedef unsigned int uint;


/*-------------------------------------
|        FUNCTION DECLARATIONS         |
--------------------------------------*/

struct Node* create_node( int val );
uint get_valid_and_null_nodes_count( struct Node* const root );
char* serialize( struct Node* const root, uint * const str_sz );
int* _serialize( struct Node* const root, int* serial_data );
struct Node* deserialize( char* const str, const uint str_sz );
struct Node* _deserialize( int** serial_data );


/*-------------------------------------
|         FUNCTION DEFINITIONS         |
--------------------------------------*/

struct Node* create_node(int val)
{
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->val = val;
    node->left = NULL;
    node->right = NULL;
    return node;
} /* create_node */

uint get_valid_and_null_nodes_count( struct Node* const root )
{
    if ( root == NULL )
    {
        return 1;
    }

    uint count = 1;

    count += get_valid_and_null_nodes_count( root->left );
    count += get_valid_and_null_nodes_count( root->right );

    return count;
} /* get_valid_and_null_nodes_count() */

/**
 * Given the root to a binary tree and a 
 * pointer to a uint, will serialize and 
 * return a string of data representing
 * the tree. Will set the uint pointer
 * given to the length of the returned
 * string.
 * 
 * @Returns a string of serialized binary tree,
 *             or NULL if failed
 *          Sets str_sz value equal to number of
 *             bytes in the char* string returned
 */

char* serialize( struct Node* const root, uint * const str_sz )
{
    if( root == NULL || str_sz == NULL )
    {
        return NULL;
    }

    *str_sz = sizeof(int)*get_valid_and_null_nodes_count( root );

    /* NOTE: str_sz should be > 0 because root is not null, therefore, there is at least one node */
    int* serial_data = (int*) malloc( *str_sz );

    _serialize(root, serial_data);

    return (char*)serial_data;
} /* serialize() */


/* This function sets all the int values in the serial_data array based on the values
    in the binary tree. root points to the current node in the tree. 
    serial_data points to the next un-used int element in the array. */
int* _serialize( struct Node* const root, int* serial_data )
{
    assert(serial_data != NULL);

    if( root == NULL )
    {
        *serial_data = NULL_MARKER;
        return &serial_data[1]; //return pointer to next free int element in array 
    }

    *serial_data = root->val;

    /* Set serial_data pointer to point to next free int element in array  */
    serial_data = &serial_data[1];

    serial_data = _serialize( root->left, serial_data );
    serial_data = _serialize( root->right, serial_data );

    return serial_data;    
} /* _serialize() */

/**
 * Given the string of a serialized
 * binary tree and its length, will 
 * create the tree represented by the
 * string and return the root of the tree
 * 
 * @Returns root node of deserialized tree,
 *          or NULL if failed
 */
struct Node* deserialize( char* const str, const uint str_sz )
{
    /* Validate Parameters */

    if ( str == NULL || str_sz == 0 )
    {
        return NULL;
    }

    /* Check if length of the given string is a valid length */
    if( 0 != str_sz%sizeof(int) )
    {
        return NULL;
    }

    /* Recreate tree from serial data */
    struct Node* root = _deserialize( (int**) &str );

    return root;
} /* deserialize() */

/* serial_data points to a pointer that points to next un-read int in
    the array. Memory is allocated individually for each node. @returns node created. */
struct Node* _deserialize( int** serial_data )
{
    assert( serial_data != NULL );
    assert( *serial_data != NULL );

    if( **serial_data == NULL_MARKER )
    {
        *serial_data = &(*serial_data)[1]; // Point serial_data pointer to the next un-read int in the array
        return NULL;
    }

    /* Increment the pointer pointed to by node to point to the next node in the array of created nodes for the tree */
    struct Node* node = create_node( **serial_data );

    /* Point *serial_data to the next un-read int in the array */
    *serial_data = &(*serial_data)[1];

    node->left  = _deserialize( serial_data );
    node->right = _deserialize( serial_data );

    return node;
} /* _deserialize() */






















/*-------------------------------------
|   CODE BELOW THIS LINE IS USED FOR   |
|    TESTING THE FUNCTION(S) ABOVE     |
--------------------------------------*/

bool compare_trees(struct Node* root1, struct Node* root2)
{
    if ( root1 == NULL && root2 == NULL )
    {
        return true;  
    } /* Trees are empty */
    else if ( root1 != NULL && root2 != NULL )
    {
        return ( root1->val == root2->val && compare_trees(root1->left, root2->left) && compare_trees(root1->right, root2->right) );
    } /* Tree's contain data */
 
    /* Else, one tree is empty, the one is not */
    return false;
} /* compare_trees() */

void destroy_tree(struct Node* node)
{
    if(node == NULL)
    {
        return;
    }

    destroy_tree(node->left);
    destroy_tree(node->right);

    free(node);
} /* destroy_tree() */

int main()
{
    #include <stdio.h>
    #include <assert.h>

    uint str_sz;
    char* str_result = NULL;
    struct Node* tree_result = NULL;
    struct Node* root = create_node(1);
 
    /* Case 1 */
    assert( NULL == serialize(NULL, NULL) );
    assert( NULL == deserialize(NULL, 0) );

    /* Case 2 */
    str_result = serialize(root, &str_sz);
    tree_result = deserialize(str_result, str_sz);
    assert( true == compare_trees(root, tree_result) );
    destroy_tree(tree_result);
    free(str_result);
    tree_result = NULL;
    str_result = NULL;

    /* Case 3 */
    root->left = create_node(2);
    str_result = serialize(root, &str_sz);
    tree_result = deserialize(str_result, str_sz);
    assert( true == compare_trees(root, tree_result) );
    destroy_tree(tree_result);
    free(str_result);
    tree_result = NULL;
    str_result = NULL;
    
    /* Case 4 */
    root->left->left = create_node(4);
    str_result = serialize(root, &str_sz);
    tree_result = deserialize(str_result, str_sz);
    assert( true == compare_trees(root, tree_result) );
    destroy_tree(tree_result);
    free(str_result);
    tree_result = NULL;
    str_result = NULL;
    
    /* Case 5 */
    root->left->right = create_node(5);
    str_result = serialize(root, &str_sz);
    tree_result = deserialize(str_result, str_sz);
    assert( true == compare_trees(root, tree_result) );
    destroy_tree(tree_result);
    free(str_result);
    tree_result = NULL;
    str_result = NULL;
    
    /* Case 6 */
    root->right = create_node(3);
    str_result = serialize(root, &str_sz);
    tree_result = deserialize(str_result, str_sz);
    assert( true == compare_trees(root, tree_result) );
    destroy_tree(tree_result);
    free(str_result);
    tree_result = NULL;
    str_result = NULL;
    
    /* Case 7 */
    root->right->right = create_node(7);
    str_result = serialize(root, &str_sz);
    tree_result = deserialize(str_result, str_sz);
    assert( true == compare_trees(root, tree_result) );
    destroy_tree(tree_result);
    free(str_result);
    tree_result = NULL;
    str_result = NULL;
    
    /* Case 8 */
    root->right->left = create_node(6);
    str_result = serialize(root, &str_sz);
    tree_result = deserialize(str_result, str_sz);
    assert( true == compare_trees(root, tree_result) );
    destroy_tree(tree_result);
    free(str_result);
    tree_result = NULL;
    str_result = NULL;
    
    /* Case 9 */
    root->left->right->left = create_node(10);
    str_result = serialize(root, &str_sz);
    tree_result = deserialize(str_result, str_sz);
    assert( true == compare_trees(root, tree_result) );
    destroy_tree(tree_result);
    free(str_result);
    tree_result = NULL;
    str_result = NULL;
    
    /* Case 10 */
    root->left->right->left->left = create_node(9);
    str_result = serialize(root, &str_sz);
    tree_result = deserialize(str_result, str_sz);
    assert( true == compare_trees(root, tree_result) );
    destroy_tree(tree_result);
    free(str_result);
    tree_result = NULL;
    str_result = NULL;
    
    /* Case 11 */
    root->right->right->right = create_node(20);
    str_result = serialize(root, &str_sz);
    tree_result = deserialize(str_result, str_sz);
    assert( true == compare_trees(root, tree_result) );
    destroy_tree(tree_result);
    free(str_result);
    tree_result = NULL;
    str_result = NULL;
    
    /* Case 12 */
    root->right->right->right->left = create_node(21);
    str_result = serialize(root, &str_sz);
    tree_result = deserialize(str_result, str_sz);
    assert( true == compare_trees(root, tree_result) );
    destroy_tree(tree_result);
    free(str_result);
    tree_result = NULL;
    str_result = NULL;

    /* cleanup */
    destroy_tree(root);

    printf("All Test Cases Successful!\n");

    return 0;
}
