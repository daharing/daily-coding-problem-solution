/*
 * Author:  Dustin Haring
 * Date:    March 31, 2020
 * Comment: This file is part of the Daily Coding Problem C solutions
 * Resource: 
 *          https://www.geeksforgeeks.org/serialize-deserialize-binary-tree/
 *          https://www.geeksforgeeks.org/tree-traversals-inorder-preorder-and-postorder/?ref=rp
 */

#include "Problem_003.h" //This include is for testing purposes only and is not part of the solution. It contains the Node struct definition
/**
 * Definition for a binary tree node (also defined in Problem_003.h)
 * struct Node {
 *     int val;
 *     struct Node *left;
 *     struct Node *right;
 * };
 */






/*-------------------------------------
|         PUBLIC DECLARATIONS          |
--------------------------------------*/

#include <stdlib.h>  //For NULL and malloc
#include <stdbool.h> //For bools
#include <assert.h>

typedef unsigned int uint;

#define LCHILD_MASK (0x01)
#define RCHILD_MASK (0x02)

#define isLChildNULL(info_byte)     ( (info_byte) & LCHILD_MASK )
#define isRChildNULL(info_byte)     ( (info_byte) & RCHILD_MASK )

#define setLChildNULL(info_byte)    ( (info_byte) |= LCHILD_MASK )
#define setLChildExists(info_byte)  ( (info_byte) &= (~LCHILD_MASK) )
#define setRChildNULL(info_byte)    ( (info_byte) |= RCHILD_MASK )
#define setRChildExists(info_byte)  ( (info_byte) &= (~RCHILD_MASK) )

struct serialNode
{
    int val;
    char info_byte; 
    /* info_byte contains 8 bits that are accessed using macros. Currently, 2 bits are used
    and they are isLeftChildNULL and isRightChildNULL accessed using the macros defined above this struct.
    info_byte is being equated to the following through macros:
    bool isLeftChildNULL;
    bool isRightChildNULL;
    */
    /* NOTE: Without using bitmasts and or macros, C assigns each bool to it's own byte/char. This is because
            C requires every variable to be able to have a pointer to it and you can't have a pointer
            to a single bit in memory. As such, these two bools cost us 2 bytes instead of 1.
            However, if you wanted to save a byte for every tree node serialized, you could use
            macros and bitmasks to have up to 8 bools stored in a single byte. */
} __attribute__((packed)); // Attribute informing compiler to pack all members (no struct padding)

/*-------------------------------------
|        FUNCTION DECLARATIONS         |
--------------------------------------*/

struct Node* create_node( int val );
uint get_node_count( struct Node* const root );
char* serialize( struct Node* const root, uint * const str_sz );
struct serialNode* _serialize( struct Node* const root, struct serialNode* serial_data );
struct Node* deserialize( char* const str, const uint str_sz );
struct Node* _deserialize( struct serialNode** serial_data );


/*-------------------------------------
|         FUNCTION DEFINITIONS         |
--------------------------------------*/

struct Node* create_node(int val)
{
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->val = val;
    node->left = NULL;
    node->right = NULL;
    return node;
} /* create_node */

uint get_node_count( struct Node* const root )
{
    if ( root == NULL )
    {
        return 0;
    }

    uint count = 1;

    count += get_node_count( root->left );
    count += get_node_count( root->right );

    return count;
} /* get_node_count() */

/**
 * Given the root to a binary tree and a 
 * pointer to a uint, will serialize and 
 * return a string of data representing
 * the tree. Will set the uint pointer
 * given to the length of the returned
 * string.
 * 
 * @Returns a string of serialized binary tree,
 *             or NULL if failed
 *          Sets str_sz value equal to number of
 *             bytes in the char* string returned
 */

char* serialize( struct Node* const root, uint * const str_sz )
{
    if( root == NULL || str_sz == NULL )
    {
        return NULL;
    }

    *str_sz = get_node_count( root )*sizeof(struct serialNode);

    /* NOTE: str_sz should be > 0 because root is not null, therefore, there is at least one node */
    struct serialNode* serial_data = (struct serialNode*) malloc( *str_sz );

    _serialize(root, serial_data);

    return (char*)serial_data;
} /* serialize() */


/* This function sets all the struct values in the serial_data struct array based on the values
    in the root struct (the binary tree data). root points to the current node in the tree. 
    serial_data points to the next available element in the array. */
struct serialNode* _serialize( struct Node* const root, struct serialNode* serial_data )
{
    assert(serial_data != NULL);

    if( root == NULL )
    {
        return serial_data;
    }

    serial_data->val = root->val;

    if( root->left == NULL )
    {
        setLChildNULL(serial_data->info_byte); // Using macro for space efficiency in serialData struct
    }
    else
    {
        setLChildExists(serial_data->info_byte); // Using macro for space efficiency in serialData struct
    }

    if( root->right == NULL )
    {
        setRChildNULL(serial_data->info_byte); // Using macro for space efficiency in serialData struct
    }
    else
    {
        setRChildExists(serial_data->info_byte); // Using macro for space efficiency in serialData struct
    }

    /* Set serial_data pointer to point to the next available/unused element */
    serial_data = &serial_data[1];

    serial_data = _serialize( root->left, serial_data );
    serial_data = _serialize( root->right, serial_data );

    return serial_data;    
} /* _serialize() */

/**
 * Given the string of a serialized
 * binary tree, will create the tree
 * represented by the string and return
 * the root of the tree
 * 
 * @Returns root node of deserialized tree,
 *          or NULL if failed
 */
struct Node* deserialize( char* const str, const uint str_sz )
{
    /* Validate Parameters */

    if ( str == NULL || str_sz == 0 )
    {
        return NULL;
    }

    /* Check if length of the given string is a valid length */
    if( 0 != str_sz%sizeof(struct serialNode) )
    {
        return NULL;
    }

    /* Allocate all the memory needed for the tree at once for efficiency */
    struct Node* root = _deserialize( (struct serialNode**) &str );

    return root;
} /* deserialize() */

/* serial_data points to a pointer that points to next un-read struct in
    the array. Memory is allocated individually for each node. @returns node created. */
struct Node* _deserialize( struct serialNode** serial_data )
{
    assert( serial_data != NULL );
    assert( *serial_data != NULL );

    bool lChildNULL = isLChildNULL( (*serial_data)->info_byte ); // Using macro for space efficiency in serialData struct
    bool rChildNULL = isRChildNULL( (*serial_data)->info_byte ); // Using macro for space efficiency in serialData struct

    /* Increment the pointer pointed to by node to point to the next node in the array of created nodes for the tree */
    struct Node* node = create_node( (*serial_data)->val );

    /* Point *serial_data to the next un-read serialNode struct in the array */
    *serial_data = &(*serial_data)[1];

    node->left  = (lChildNULL == true)? NULL : _deserialize( serial_data );
    node->right = (rChildNULL == true)? NULL : _deserialize( serial_data );

    return node;
} /* _deserialize() */
























/*-------------------------------------
|   CODE BELOW THIS LINE IS USED FOR   |
|    TESTING THE FUNCTION(S) ABOVE     |
--------------------------------------*/

bool compare_trees(struct Node* root1, struct Node* root2)
{
    if ( root1 == NULL && root2 == NULL )
    {
        return true;  
    } /* Trees are empty */
    else if ( root1 != NULL && root2 != NULL )
    {
        return ( root1->val == root2->val && compare_trees(root1->left, root2->left) && compare_trees(root1->right, root2->right) );
    } /* Tree's contain data */
 
    /* Else, one tree is empty, the one is not */
    return false;
} /* compare_trees() */

void destroy_tree(struct Node* node)
{
    if(node == NULL)
    {
        return;
    }

    destroy_tree(node->left);
    destroy_tree(node->right);

    free(node);
} /* destroy_tree() */

int main()
{
    #include <stdio.h>
    #include <assert.h>

    uint str_sz;
    char* str_result = NULL;
    struct Node* tree_result = NULL;
    struct Node* root = create_node(1);
 
    /* Case 1 */
    assert( NULL == serialize(NULL, NULL) );
    assert( NULL == deserialize(NULL, 0) );

    /* Case 2 */
    str_result = serialize(root, &str_sz);
    tree_result = deserialize(str_result, str_sz);
    assert( true == compare_trees(root, tree_result) );
    destroy_tree(tree_result);
    free(str_result);
    tree_result = NULL;
    str_result = NULL;

    /* Case 3 */
    root->left = create_node(2);
    str_result = serialize(root, &str_sz);
    tree_result = deserialize(str_result, str_sz);
    assert( true == compare_trees(root, tree_result) );
    destroy_tree(tree_result);
    free(str_result);
    tree_result = NULL;
    str_result = NULL;
    
    /* Case 4 */
    root->left->left = create_node(4);
    str_result = serialize(root, &str_sz);
    tree_result = deserialize(str_result, str_sz);
    assert( true == compare_trees(root, tree_result) );
    destroy_tree(tree_result);
    free(str_result);
    tree_result = NULL;
    str_result = NULL;
    
    /* Case 5 */
    root->left->right = create_node(5);
    str_result = serialize(root, &str_sz);
    tree_result = deserialize(str_result, str_sz);
    assert( true == compare_trees(root, tree_result) );
    destroy_tree(tree_result);
    free(str_result);
    tree_result = NULL;
    str_result = NULL;
    
    /* Case 6 */
    root->right = create_node(3);
    str_result = serialize(root, &str_sz);
    tree_result = deserialize(str_result, str_sz);
    assert( true == compare_trees(root, tree_result) );
    destroy_tree(tree_result);
    free(str_result);
    tree_result = NULL;
    str_result = NULL;
    
    /* Case 7 */
    root->right->right = create_node(7);
    str_result = serialize(root, &str_sz);
    tree_result = deserialize(str_result, str_sz);
    assert( true == compare_trees(root, tree_result) );
    destroy_tree(tree_result);
    free(str_result);
    tree_result = NULL;
    str_result = NULL;
    
    /* Case 8 */
    root->right->left = create_node(6);
    str_result = serialize(root, &str_sz);
    tree_result = deserialize(str_result, str_sz);
    assert( true == compare_trees(root, tree_result) );
    destroy_tree(tree_result);
    free(str_result);
    tree_result = NULL;
    str_result = NULL;
    
    /* Case 9 */
    root->left->right->left = create_node(10);
    str_result = serialize(root, &str_sz);
    tree_result = deserialize(str_result, str_sz);
    assert( true == compare_trees(root, tree_result) );
    destroy_tree(tree_result);
    free(str_result);
    tree_result = NULL;
    str_result = NULL;
    
    /* Case 10 */
    root->left->right->left->left = create_node(9);
    str_result = serialize(root, &str_sz);
    tree_result = deserialize(str_result, str_sz);
    assert( true == compare_trees(root, tree_result) );
    destroy_tree(tree_result);
    free(str_result);
    tree_result = NULL;
    str_result = NULL;
    
    /* Case 11 */
    root->right->right->right = create_node(20);
    str_result = serialize(root, &str_sz);
    tree_result = deserialize(str_result, str_sz);
    assert( true == compare_trees(root, tree_result) );
    destroy_tree(tree_result);
    free(str_result);
    tree_result = NULL;
    str_result = NULL;
    
    /* Case 12 */
    root->right->right->right->left = create_node(21);
    str_result = serialize(root, &str_sz);
    tree_result = deserialize(str_result, str_sz);
    assert( true == compare_trees(root, tree_result) );
    destroy_tree(tree_result);
    free(str_result);
    tree_result = NULL;
    str_result = NULL;

    /* cleanup */
    destroy_tree(root);

    printf("All Test Cases Successful!\n");

    return 0;
}
