/*
 * Author:  Dustin Haring
 * Date:    March 29, 2020
 * Comment: This file is part of the Daily Coding Problem C solutions
 * Resource: 
 */

/**Due to the potentially complicated nature of this problem
 * I have provided 2 examples and what the arrays should look
 * like in the solution function.
 * 
 *  list 1  ,2 ,3 ,4  ,5
 *  lr   1  ,2 ,6 ,24 ,120
 *  rl   5  ,20,60,120,120
 *  
 *  ans  120,60,40,30 ,24
 *  
 *  
 *  list 10 ,5  ,3   ,7
 *  lr   10 ,50 ,150 ,1050
 *  rl   7  ,21 ,105 ,1050
 *  
 *  ans  105,210,350 ,150
 * 
 * As you can see, the last two values in the products_lr and
 * products_rl arrays above are the same. As such, these two
 * arrays account for this by not including that index for
 * memory conservation. (helpful note, products_lr => multiplies left to right
 * and products_rl => multiplies right to left. They are named according to the
 * order in which the values in the given array (list[]) are multiple).
 */


/*-------------------------------------
|         PUBLIC DECLARATIONS          |
--------------------------------------*/
#include <stdlib.h> //For NULL and malloc
typedef unsigned int uint;


/*-------------------------------------
|              FUNCTIONS               |
--------------------------------------*/

/**
 * Given a list of input values and its length,
 * creates a new array where each value at each
 * index is the product of all values in the
 * original array except the value at the current
 * index
 * 
 * @Returns a new array with expected values
 */

int* get_product_arry( int list[], uint list_sz )
{
    /* Validate Parameters */

    if( list == NULL || list_sz < 2 )
    {
        return NULL;
    }


    /* Declare Variables */

    int products_lr[list_sz-1];//Products from left to right of list[]
    int products_rl[list_sz-1];//Products from right to left of list[]


    /* Allocate new arry that will be returned */
    int *new_arry = malloc(list_sz);//(Malloc shouldn't be called with a value of '0')


    /* Set values of products arrays */

    products_lr[0] = list[0];          //Init to first value/factor in list[]
    products_rl[0] = list[list_sz-1];  //Init to last value/factor in list[]

    for (uint i = 1; i < list_sz-1; i++)
    {
        /* For each iteration, take the value in products arrays and set it equal to the previous value 
        (which is a product of all previous products) times the next factor in list[] */
        products_lr[i] = products_lr[i-1] * list[i]; //Set this index value to the product of all values to the left of this index in list[]
        products_rl[i] = products_rl[i-1] * list[(list_sz-1)-(i)]; //Set this index value to the product all the values to the right of this index in list[]
    }


    /* Determine each value in new_arry by multiplying appropiate products from list[] that are 
        already multiplied and stored in the factor arrys */

    new_arry[0] = products_rl[(list_sz-1)-1];
    new_arry[list_sz-1] = products_lr[(list_sz-1)-1];

    for (uint i = 1; i < list_sz-1; i++)
    {
        /* To get all the 'products' from the original array, we
        use the products_lr array to get all products from the left
        side of the current index value in list[] (the original array), 
        and we use the products_rl array to get all the products to
        right of the current index from the original array list[]. 
        
        Of course, this loop can only handle all but the first and last indices due to the index math becoming negative */
        new_arry[i] = products_lr[i-1] * products_rl[(list_sz-1)-(i+1)]; //Multiple all values in list[] except the value at index 
    }
    
    return new_arry;
} /* get_product_arry() */










/*-------------------------------------
|   CODE BELOW THIS LINE IS USED FOR   |
|    TESTING THE FUNCTION(S) ABOVE     |
--------------------------------------*/
int main()
{
    #include <stdio.h>
    #include <assert.h>

    int* new_arry;

    /* Test Case 1 */
    int list[] = {1,2,3,4,5};
    int answer[] = {120,60,40,30,24};
    new_arry = get_product_arry(list, 5);
    for (size_t i = 0; i < 5; i++)
    {
        assert(new_arry[i] == answer[i]);
    }
    free(new_arry);

    /* Test Case 2 */
    int list2[] = {10, 5, 3, 7};
    int answer2[] = {105, 210, 350, 150};
    new_arry = get_product_arry(list2, 4);
    for (size_t i = 0; i < 4; i++)
    {
        assert(new_arry[i] == answer2[i]);
    }
    free(new_arry);

    /* Test Case 3 */
    int list3[] = {8, 4};
    int answer3[] = {4, 8};
    new_arry = get_product_arry(list3, 2);
    for (size_t i = 0; i < 2; i++)
    {
        assert(new_arry[i] == answer3[i]);
    }
    free(new_arry);

    /* Test Case 4 */
    int list4[] =   {7};
    new_arry = get_product_arry(list4, 1);
    assert(new_arry == NULL);

    /* Test Case 5 */
    new_arry = get_product_arry(NULL, 0);
    assert(new_arry == NULL);

    printf("All Test Cases Successful!\n");

    return 0;
}
