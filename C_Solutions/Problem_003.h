struct Node 
{
    int val;
    struct Node *left;
    struct Node *right;
};
