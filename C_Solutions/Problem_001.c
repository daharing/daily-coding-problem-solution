/*
 * Author:  Dustin Haring
 * Date:    March 28, 2020
 * Comment: This file is part of the Daily Coding Problem C solutions
 * Resource: 
 *              https://stackoverflow.com/questions/6727136/performance-impact-of-realloc
 * 
 */


/*-------------------------------------
|               INCLUDES               |
--------------------------------------*/
#include <stdio.h>
#include <stdbool.h>

typedef unsigned int uint;

/*-------------------------------------
|              FUNCTIONS               |
--------------------------------------*/

/**
 * Given a list of integers, the length of the list,
 * and an integer k,
 * 
 * will check the list to see if any two numbers
 * in the list sum to k.
 * 
 * @Returns True if true if two numbers in list sum to k
 *          False otherwise
 */

bool Check_If_Sums( int list[], uint list_sz, int k )
{
    int possible_solns[list_sz]; /* NOTE This list could be optimized using an exponential size realloc */
    uint possible_solns_sz = 0;

    /* Validate Parameters */
    if( list == NULL || list_sz < 2 )
    {
        return false;
    }

    /* Determine if value k exists of two indicies summed */
    int value;
    uint i,j;
    for( i = 0; i < list_sz; i++ )
    {
        value = k - list[i];

        /* Check if value exists in possible_solns[] */
        for( j = 0; j < possible_solns_sz; j++ )
        {
            if( possible_solns[j] == list[i] )
            {
                return true;
            }
        }

        /* If value was not found in possible_solns[], add it to the array */
        if( j == possible_solns_sz )
        {
            possible_solns[j] = value;
            possible_solns_sz++;
        }
        
    }

    return false;
} /* Check_If_Sums() */


/*-------------------------------------
|   CODE BELOW THIS LINE IS USED FOR   |
|    TESTING THE FUNCTION(S) ABOVE     |
--------------------------------------*/
int main()
{
    #include <assert.h>

    int list[] = {10, 15, 3, 7};
    assert( Check_If_Sums(list, 4, 17) == true );

    assert( Check_If_Sums(list, 1, 10) == false );
    assert( Check_If_Sums(NULL, 0, 10) == false );

    int list2[] = {10, 15, 0, 3, 7};
    assert( Check_If_Sums(list2, 5, 17) == true );

    int list3[] = {10, 15, 0, 3, 7};
    assert( Check_If_Sums(list3, 5, 0) == false );

    int list4[] = {10, 15, 0, 3, -15};
    assert( Check_If_Sums(list4, 5, 0) == true );

    printf("All Test Cases Successful!\n");

    return 0;
}
